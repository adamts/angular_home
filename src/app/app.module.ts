import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import { PostsService} from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostFormComponent } from './post-form/post-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import{AngularFireModule} from 'angularfire2';

export const firebaseConfig = {

    apiKey: "AIzaSyCnNhVCNpOqzO3OsCL7RPZSC_QEm0zoSSw",
    authDomain: "adam-a5f43.firebaseapp.com",
    databaseURL: "https://adam-a5f43.firebaseio.com",
    storageBucket: "adam-a5f43.appspot.com",
    messagingSenderId: "780221808616"
  

}







const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: '', component: PostsComponent },
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    PostFormComponent,
    PageNotFoundComponent,
    UserFormComponent
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],

  
  providers: [PostsService],
  bootstrap: [AppComponent]

})
export class AppModule { }
